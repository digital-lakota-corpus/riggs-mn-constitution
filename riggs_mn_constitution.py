# Copyright 2022 Digital Lakota Corpus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

SOURCE_TEXT = """
# MINNESOTA MAKOCE EN WOOPE ITANCAN KIN

Minnesota Makoce kin en wicaxta unyakonpi,
Wakantanka wopida unkekiciyapi, woihduha wakan qa
ikceka unhapi kin heon; qa wowaxte kin he ohinniyan
kta, unkiyepi qa unkicincapi kin hena en unkiçihdusutapi
kta uncinpi, heon etanhan Woope Itancan kin de unkagapi
qa unhdusutapi.

TOKAHEYA.

WIHDUHAPI WOWAPI.

Oehde 1. Oyate kin ixnana woope ikceka kaga okihipi,
qa hena tanyan unpi qa owotanna oranyanpi kta
e heon Wokiconze kagapi ece; qa tohan econpi kta
iyecece cinhan Wokiconze kin he piyapi qa yutecapi kta
ixnana okihipi.

Oehde 2. Makoce kin de en tuwedan reyata iyeyapi
kte xni, qa tona wicaxta wicayawapi token wihduhapi qa
wicoran econpi ece kin wicakipi kte xni, makoce kin
en woope yuhapi kin eciyatanhan xni, qa wicaxta om
akiniskokeca kin on yacopi xni kinhan. Makoce kin
de en Waxicun sapa iyecen wayaka wicayuhapi kte xni,
qa tuwe tawatenye xni wowidake yapi kte xni; tuka
tuwe taku xica econ kinhan wanna on yacopi hecinhan he
kakixyapi kta e on kaxka hnakapi qa wowidake yapi kta.

Oehde 3. Wowapi maza on kagapi kin he ohinniyan
hecetu kta, qa wicaxta owasin taku owasin on, token
iyokipipi, icakijexniyan iapi qa wowapi kagapi qa hduotaninpi
okihipi kta; tuka tona xicaya iapi qa wowapi kagapi
kinhan, henana on kakixwicayapi kta.

Oehde 4. Wicaxta karnir wiyukcanwicakiyapi kte
cin he ohinniyan hecetu kta; qa taku akinicapi
kin owasin en ayepica kta; tuka anokatanhan cinpi kinhan
wiyukcanwicakiyapi yuhapi kte xni, woope en kagapi
kin iyececa.

Oehde 5. Woehnake ota rinca kapi kte xni, qa xicaya
econpi kin on mazaska ota rinca wicakipi kte xni, qa
iyotan onxiya kakixwicayapi kte cin wanice kta.

Oehde 6. Tuwe xicaya oranyan keya iyaonpapi hecinhan,
makoce qa otonwe kin tukte en econ keyapi, he
en kohanna wicaxta om akiniskokeca iyukcanwicakiyapi
kta; qa taku econ keyapi kin he tokaheya okiciyakapi
kte ça, tona sdonyapi hena itkokim woyagwicakiyapi kta,
qa nakun iye cin kinhan woope-aie cin heca wawiciya
yuhe kta.

Oehde 7. Wicaxta wiyukcanpi tanka kin hena tokaheya
akitapi qa taku iyeyapi xni kinhan tuwedan taku
xica econ keyapi kin on yacopi kta e on wohdagkiyapi
kte xni; tuka okicize kaga iyaonpapi, qa taku tona
Wookiye Wiyukcan yaco kte cin hena, qa akicita oranpi
kin hena deciyatanhan ope kte xni. Tuwe taku xica
wanjidan econ kin on nonpa yacopi kte xni; tuwedan
taku xica econ kin on ihdacokiyapi kte xni; qa tuwedan
woope eciyatanhan yacopi xni kinhan, ktepi kte xni qa
kaxka hnakapi kte xni qa wakipi kte xni. Tuwe hinarin
yacopi xni kinhan kiyuxpepica kta, wicaxta owotanna
wakicihnakapi kinhan, qa taku xica econ kin on ktepi
kta iyecece xni kinhan; qa iye iyakitedan kicizapi xni,
qa ix ahiwicaranpi xni kinhan wicaxta kiyuxpepica wowapi
kin he yujujupi kte xni.

Oehde 8. Wicaxta otoiyohi taku xica ecakiconpi, iye
tan atayedan qa ix taku tawa kin en, heon woope eciyatanhan
owotanna ecakiconpi kta iyececa; qa he inyunwin
codan ocowasin orankoya ecakiconpi kta iyececa.

Oehde 9. Makoce kin de ohna tuwe oyate kin azuwicaye
ça, tona tokayapi kin iwicakiye ça owicakiye cin he
wicoran xica wan tanka econ kta; tuka tuwedan heon
yacopi kte xni, wicaxta nom oyakapi xni, qa ix, iye
woyaco tipi kin ohna taninyan ohdake xni kinhan.

Oehde 10. Wicaxta owasin, iye tan atayedan yutanpica
xni unpi kta, tipi tawapi, qa wowapi warpaya ko
yuhapi kin hena, woope eciyatanhan eceedan yutanpica
qa akitepica kta; qa taku akitapi kte ça icupi kte cinhan,
he tukten iyeyapi kta naceca hecinhan tanyan sdonyapi
qa oyakapi xni kinhan woicu wowapi kagapi kte xni.

Oehde 11. Taku xica econpi kin iyohakam wokonze
woope tohinni on kagapi kte xni, qa oicazo yuecetu xni
woope icimana kagapi kte xni, qa tuwe yacopi exta heon
we xica icage kte xni, qa on taku yuhe ciqon tawa xni
kiyapi kte xni.

Oehde 12. Makoce kin de ohna tuwedan oicazo
hdajuju xni on kaxkapi kte xni, tuka tuwe hnaye taku
icazo keya iyaonpapi kin he kaxka hnakapi kte, ça ix,
on waehnag wicakiyapi kta woope kagapi kta Woope
kaga omniciye kin okihipi kta. Oicazo kajujupi kta e
warpaya apa icupi kte xni; qa tona hecen un kte cin he
woope eciyatanhan wakiconzapi kta.

Oehde 13. Taku tuwe atayedan yuhe cin he oyate
kin icupi kte xni, tokaheya opetonpi qa owotanna kajujupi
xni kinhan.

Oehde 14. Akicita kin owasin Wicaxtayatapi kin
ihukuya unpi kta, qa wookiye unpi kinhan icunhan
Makoce kin de en akicita on zuyapi kte cin wicayuhapi
kte xni.

Oehde 15. Makoce kin de en maka kin wicaxta
adatayedan tawapi kta. Qa detanhan maga odon opetonpi
kin waniyetu wikcemna nonpa sanpa wanjidan hehanyan
econpica kta.

Oehde 16. Woope Itancan kin de en woihduhapi
tona cajeyatapi kin hena tona wicaxta owasin ton icagapi
kin hena yuecetupi kte xni tuwedan ecin kte xni.
Wicaxta otoiyohi, token iyecetudake cinhan, hecen
Wakantanka ohoda kta, qa he ohinniyan hecetu kta.
Tipi wakan kagapi qa en opapi kta tuwe tawatenye xni
kinhan he en opekiyapi kte xni; qa woope eciyatanhan
tipi wakan wanjidan eciyatanhan opapi kin iyotan waxte
yawapi kte xni; qa wicowiyukcan eciyatanhan opapi
kin he tohinni yuecetupi kte xni; tuka wicowiyukcan
eciyatanhan opapi kin detanhan yusutapi kin he taku
xica econpi kin on akarpetonpi kte xni, qa taku econpi
on oyate kin tanyan unpi kte xni kin hena owotanna
yawapi kte xni.

Oehde 17. Makoce kin de en tuwe wohduze yuhe, ça
ix yuhe xni exta, qa tuwe woyuha ton, qa ix ton xni exta,
wicaxtayatapi karnirpica kta; qa tuwe kaxta wohduze
yuhe xni qa woyuha kanakeca yuhe xni exta wicaxtayatapi
karnir unpi kin en ope kta; qa wicaxta token Taku
Wakan cantekiyuza exta, he on Woyaco tipi kin ohna
wayaotanin kta terindapi kta ecinpica kte xni.

# ICINONPA.

## MAKOCE CAJE QA IHDUKXAN KIN.

Oehde 1. Makoce kin de _Minnesota Makoce_ eciyapi
kta; qa makoce kin de hinskoya kta: Waziyata ekta
Sagdaxa tawakpa kin tukten Sagdaxa qa Isantanka
makoce icagopi kin wakpa kin he ipawer wanke cin
mini cokaya kin hetanhan wakpa ohna tatowam aupi kta,
ecen Can-inkpa wakpa mdote kin ehan ahipi, qa wakpadan
kin he ohna aupi kta ecen Mde Hdakinyan hiyahde
kta, qa Mde kin he mini cokaya ohna aupi kta ecen
Ptansinta en ahihnakapi kta; qa hetanhan owotanna
aupi kta ecen Mdeipakxan ihanke kin hiyahde kta, qa
Mde kin he ohna mini cokaya aupi kta, ecen Mde
owihanke kin ekta ahihnakapi kta; qa hetanhan owotanna
itokaga ayapi kta, ecen Iowa makoce kin iyahde
kta, qa makoce kin he icagopi ohna wiyohiyanpatakiya
ayapi kta, ecen Rara wakpa hiyohi kta, qa hetanhan
Rara wakpa ocokaya ohna tatowam aupi kta Hogan-wanke
mdote hehanyan, qa Hogan-wanke ohna ayapi kta,
qa Wiskonsan makoce icagopi kin ohna ayapi kta, ecen
Saint Louis wakpa kin iyorpaye kta, qa wakpa kin he
ohna qa Mde Tanka opta ayapi kta, ecen Wakiyedan
tawakpa kin iyorpaye kta, qa wakpa kin he ohna ayapi
kta, qa Sagdaxa Isantanka kici makoce icagopi kin ohna
ayapi kta, ecen totanhan aupi qon he iyorpaye kta.

Oehde 2. Minnesota Makoce kin Rara wakpa qa
wakpa toktokeca tona makoce tokeca kici ohna icagopi
kin hena om tawa kta; qa wakpa kin hena qa tona
etanhan uye cin hena oyate ocowasin tawapi kta; tuka
wicaxta Minnesota en unpi qa Isantanka iyurpa hena
ohna inyunwin codan ohnihdapi ece e kta.

Oehde 3. Isantanka Omniciye Tanka woope kagapi
qon, "Minnesota Makoce kin en oyate kin Woope
Itancan kagapi kte ça on Isantanka Oyate tokaheya
kagapi kin om akiyececa yawapi kta" e wakiconzapi kin
he tona en eyapi kin hena nakaha unkicupi qa unyusutapi,
qa hena hecetu kta, Isantanka Oyate ocowasin
yujujupi xni kinhan. Unkan oyate kin de Isantanka
Oyate kin iyurpa makoce kin de en maka token wiyopeye
kte cin tohinni i en hiyeye kte xni, qa Omniciye Tanka
token wicaxta tan atayedan makoce yuhapi kin wicaqu
kta e wakiconzapi kinhan nakun i en hiyeye kte xni; qa
makoce kin tohanyan Isantanka Oyate ocowasin tawa
kinhan he omaka eca wokajuju wanice kta, qa tona en
tipi xni makoce yuhapi, en ounyanpi kin om akidecen
omaka eca wokajuju wicakicagapi kta ece.

# ICIYAMNI.

## WOKICONZE WOWAXAKE KIN.

Oehde. Wokiconze wowaxake kin yamnikiya oranyanpi
kta; Woope kagapi, Waeconwicakiyapi, qa Woyaco,
hena yamni; qa wicaxta tona unma tukte wanji en
opapi kin hena unmapi kin en takudan econpi kte xni,
Woope Itancan kin de en tona econ wicaxipi kin henana
econpi kta.

# ICITOPA.

## WOOPE KAGAPI KIN.

Oehde 1. Woope kagapi kte cin Makoce kin de en
Omniciye nonpa kta, unma Oksape eciyapi kta, qa unma
kin ix Wicakarnigapi eciyapi kta; qa hena tohanhan
woope eciyatanhan econ wicaxipi eca Otonwe Itancan
kin ohna mniciyapi ece e kta.

Oehde 2. Oksape qa Wicakarnigapi kin en wicaxta
tonakecapi kta he woope on wakiconzapi kta; tuka
Oksape en opapi kte cin makoce kin en ounyanpi kektopawinge
zaptan on wicaxta wanji karnigapi kta, qa
tohinni sanpa kte xni; qa Wicakarnigapi kin en opapi
kte cin makoce kin en ounyanpi kektopawinge nonpa on
wicaxta wanjidan sanpa kte xni. Qa makoce owancaya
tona en ounyanpi kin iyecen Oksape qa Wicakarnigapi
kin en wicaxta hiyuwicayapi ece e kta, tuka Ikcewicaxta
woope eciyatanhan omaka eca wokajuju econwicakiyapi
xni hena wicayawapi kte xni.

Oehde 3. Omniciye kin hena oza kinnukankiya
wicaxta wicakarnigapi qa en hiyuwicayapi kin wicayukcanpi,
qa owotanna wicakarnigapi hecinhan he iyukcanpi
kta. Hanke isanpa witaya yukanpi kinhan taku econ
okihipi kta, tuka iyotpani en unpi kinhan anpetu otoiyohi
ake kihnakapi kta, qa tona hipi xni kin hena token
uwicakiyapi kta e wakiconza okihipi kta.

Oehde 4. Omniciye kin hena otoiyohi token oranyanpi
kta hdukcanpi qa wakiconzapi kta; ito ayuxtanpi qa ake
mniciyapi ece e kta; iyepi etanhan tuwe xicaya econ
hecinhan kakixyapi kta okihipi, qa nakun reyata iyeyapi
kta okihipi, wicaxta yamnimni etanhanhan nomnom hecen
wakiconzapi kinhan; tuka tuwedan taku wanjidan on
nonpa reyata iyeyapi kte xni.

Oehde 5. Wicakarnigapi Omniciye kin wicaxta wanji
Itancan yuhapi kte cin he hdarnigapi kta; qa omniciye
napin woope eciyatanhan tona ookiye wicayuhapi kte cin
hena wicakarnigapi kta; napin taku tona econpi kin
anpetu otoiyohi Wowapi en kagapi kte ça tohan cinpi
eca hena yuotaninpi ece kta; qa taku akinicapi eca,
tona _Han_ qa _Hiya_ eyapi kin owasin ecen Wowapi kin
he en kagapi kta.

Oehde 6. Omniciye kin hena tohanyan enakiyapi xni
hehanyan unmana anpetu yamni, anpetu wakan ope kte
xni, sanpa acakxin kte xni, qa tokan emniciye kte
xni, unma kin iwangapi xni qa wicada xni kinhan.

Oehde 7. Oksape qa Wicakarnigapi kin hena tokaheya
mniciyapi kin icunhan wicaxta otoiyohi anpetu iyohi
mazaska yamnimni yuhapi kta; tuka iyohakam woope
eciyatanhan sanpa karpica kta. Tuka sanpa kagapi
exta Wicakarnigapi eciyapi en opapi kin tohanyan wicaxta
kin hena wicakarnigapi kin ihunniyanpi xni hehanyan
ecetu kte xni.

Oehde 8. Omniciye kin dena tona en opapi kin hena
owasin tohanyan witaya unpi, qa ekta yapi qa etanhan
kupi kin hehanyan, wicayuzapi qa kaxka wicahnakapi
kte xni; tuka okicize kagapi qa ix tin wicaktepi kinhan
on wicakaxkapi kta. Qa mniciyapi kin en taku eyapi
kin on tokan wiwicawangapi kte xni.

Oehde 9. Oksape qa Wicakarnigapi kin tohanyan en
opapi kte cin hehanyan Isantanka Oyate kin etanhan qa
Minnesota Oyate kin etanhan oun kin tokeca en unpi kte
xni, tuka wowapi ayekiyapi heceedan koya yuha okihipi
kta; qa tohanyan Woope kaga omniciye kin en opapi
kin icunhan oun kin tona tokaheya kagapi, qa wokajuju
sam yuotapi kin hena wanjidan yuhapi kte xni; unma
enakiyapi qa waniyetu wanjidan iyohakam hehanyan.

Oehde 10. Woope on mazaska mnayanpi kin owasin
Wicakarnigapi Omniciye kin en tokaheya kagapi kta,
tuka Oksape kin piya kagapi qa om yusutapi kta okihipi,
woope tokeca iyececa.

Oehde 11. Woope wowapi otoiyohi, tohan Oksape qa
Wicakarnigapi kin napin tanyan yacopi qa yuxtanpi
kinhan, hehan Minnesota en Wicaxtayatapi Itancan kin
kaipi kta. He waxtedake cinhan yutan kte ça Minnesota
Makoce kin en Wowapi kaga Itancan yuhapi kin he ti
kin ohna ehnake kte, ça Omniciye tipi wanji tukte en
tokaheya icage cin hen owicakiyake kta, hehan woope
kin yuxtanpi kta. Tuka woope kin he Wicaxtayatapi
Itancan kin iyokipi xni kinhan, taku tona on iyokipi xni
kin wowapi en kage kta, qa hena woope wowapi kin
iyahna, tipi wanji tukte en tokaheya kagapi kin hen
wicakicu kta; unkan taku on iyokipi xni qon hena iye
tawowapi kin en owapi kta, hehan woope kin piya rin
aiapi kta. He piya aiapi kinhan wicaxta yamnimni
etanhanhan nomnom cinpi kinhan hecen ake wakiconzapi
kte ça, hehan ti unma kin ekta aipi kta; qa hehan
hena ix piya aiapi kte ça yamnimni etanhanhan nomnom
cinpi kinhan hecen woope kta. Tuka heconpi eca tipi
napin ohna, tona _Han_ eyapi, qa tona _Hiya_ eyapi kin
owasin wowapi kin en owapi kta. Woope wowapi wanji
yamni can icunhan, anpetu wakan ope kte xni, Wicaxayatapi
Itancan kin wicakicu xni kinhan, he yutan xni
exta woope kta; tuka anpetu yamni kin hena en Woope
kaga omniciye kin enakiyapi qa kihdapi kinhan hehan
he woope kte xni. Qa ehake anpetu yamni taku tona
wakiconzapi kinhan, he iyohakam anpetu yamni hehanyan
Wicaxtayatapi Itancan kin wowapi kin hena yutan kte
ça, Wowapi kaga Itancan ti kin en ehnake kta.

Oehde 12. Mazaska taku on iyog ehnakapi kte cin
owasin woope wakiconzapi on econpi ece e kta. Taku
tona wakiconzapi kin ocaje owasin, omniciye napin
econpi kin, hena Wicaxtayatapi Itancan yutan kta e on
ekta aipi kta; tona yutan kin hena hecetu kte, ça tona
yutan xni hena ake piya aiapi qa yamnimni etanhanhan
nomnom cinpi kinhan yusutapi kta; tuka omniciye kin
enakiyapi kta wakiconzapi kin he Wicaxtayatapi Itancan
kin kaipi kte xni.

Oehde 13. Makoce kin de en woope yuxtanpi owasin,
"Minnesota Makoce kin en Woope kaga omniciye kin
wakiconzapi ce," eya en owapi kta. Qa wicaxta Omniciye
napin en opewicakiyapi kin etanhan hanke sanpa
cinpi xni qa wowapi kin en owapi xni kinhan woope
takudan wakiconzapi kte xni.

Oehde 14. Wicakarnigapi omniciye kin he wiyaonpapi
kin owasin akitapi qa cajeyatapi kta, en iyotankapi ece
kin etanhan wicota wicadapi kinhan. Qa wiyaonpapi
kin owasin Oksape kin yacopi kta; qa tohan wayaco
iyotankapi kinhan woope qa wootanin kin eciyatanhan
owotanna yacopi kta atayedan keyapi kta. Unkan
Oksape en unpi kin yamnimni etanhanhan nomnom
heyapi xni kinhan tuwedan yacopi kte xni.

Oehde 15. Tuwe woitonxni içicage, ça ix mazaska
on iyopeiçiye ça taku xica tokeca econ, yuotaninpi
kinhan, he Woope kaga omniciye kin reyata iyeya okihipi
kta, hecen wicaxta wicakarnigapi kin en ope kte xni.

Oehde 16. Woope kaga omniciye unma tukte en
wicaxta opapi kin etanhan nom, yamni qa sanpa, taku
wakiconzapi kin xica kecinpi kinhan i en iyeyapi kta
okihipi, qa i en iyeyapi taku on etanhan koya anpetu
wowapi kin en kagapi kta.

Oehde 17. Woope kaga omniciye napin tohan wicaxta
wanji en un xni kinhan Wicaxtayatapi Itancan kin
wowapi kage kta, qa heon wicaxta tokeca wanji karnigapi
kta. Qa wicaxta nom wicakarnigapi kecinpi qa oiyotanke
wanjidan en iyotankapi kta akinicapi hecinhan,
unma tukte en ope kta token yuotaninpi kta e he Woope
kaga omniciye kin woope on wakiconzapi kta.

Oehde 18. Tuwe tokeca Woope kaga omniciye tipi
kin en i qa wicaxtaxniyan oranye ça owodutaton kage
cinhan he kaxka hnaka okihipi kta, tuka anpetu hanyetu
iyahna hehanyan kaxkapi yanke kta.

Oehde 19. Omniciye kin hena napin taku aiapi qa
wakiconzapi icunhan tuwe kaxta en i kta okihi, tuka
tohan taku narmana aiapi kta iyecece dakapi kinhan
tiyopa nakitakapi kta.

Oehde 20. Woope wowapi otoiyohi omniciye kin
kinukankiya anpetu yamni en yawapi kta; tuka taku on
inarnipi kta iyececa kecinpi kinhan wicaxta yamnimni
etanhanhan nomnom cinpi kinhan hecen econpi kte xni.
Qa woope wowapi kin omniciye napin en nonpakiya
ocowasin ihunniyan taninyan yawapi xni kinhan wakiconzapi
kte xni.

Oehde 21. Woope wowapi otoiyohi omniciye napin
en yuxtanpi kinhan hehan woope owapi ece kin en owapi
kta, qa omniciye kin en Itancan wicayuhapi kin napin
yutanpi kta. Qa Itancan wicayuhapi kin wanji woope
wowapi wanna omniciye napin yuxtanpi kinhan he yutan
kta wicada xni kinhan, hetanhan Woope kaga omniciye
kin en ope kta okihi kte xni, qa nakun Minnesota
Makoce kin en woyuonihan qa wokamna ounyanpi
wanjidan yuhe kte xni. Qa woope wowapi Itancan kin yutan
xni kinhan, token yutanpi kte ça Wicaxtayatapi
Itancan kaipi kta he woope on omniciye kin kinukankiya,
wakiconzapi kta.

Oehde 22, Anpetu wan en Omniciye kin napin
enakiyapi kta wakiconzapi kin he ohna woope wowapi
wanjidan wakiconzapi kte xni. Tuka hececa exta
anpetu kin he en woope owapi ece kin en owapi kta, qa
yutanpi kte ça Omniciye unma ekta aipi kta, qa ix sanpa
oyakapi kta, qa Wicaxtayatapi Itancan yutan kta e ekta
aipi kta, hena owasin econpica kta.

Oehde 23. Minnesota Makoce kin en oyate kin owasin
owicawapi kta Woope kaga omniciye kin woope on
token econpi kta he wakiconzapi kta, waniyetu 1865, he
en econpi kta, qa hetanhan waniyetu wikcemnamna ake
econpi ece e kta. Oyate oiçiwapi qa nakun Isantanka
Wicaxtayatapi kin owicawa kinhan, iwaniyetu mniciyapi
hehan Woope kaga omniciye kin Isantanka Omniciye
tanka en opapi kte, ça iyepi Oksape qa Wicakarnigapi
kin en opapi kta wicakarnigapi kta on makoce tinskoskoya
etanhan iyaye wicayapi kta he piya aiapi qa yucopi
kta, Woope Itancan kin de eciyatanhan.

Oehde 24. Tona Wicakarnigapi omniciye kin en
opapi kte cin tohan wicakarnigapi eca Oksape kin ix
eya wicakarnigapi kta ece; qa Oksape makoce etanhanhan
wicakarnigapi kte cin hena kagapi kinhan Wicakarnigapi
ewicakiyapi makoce kin etanhan icupi ece kin
wanjidan kiyuxpapi kte xni. Oksape makoce etanhanhan
wicakarnigapi kin hena owecinhanhan yawapi kta,
qa tona tokaheya, iyamni, izaptan, ixakowin, qa hecen
ayapi kin, hena waniyetu wanjidan hehan enakiyapi kta,
hehan tokeca wicakarnigapi kta; qa tona inonpa, itopa,
ixakpe, qa hecen ayapi, etanhanhan wicakarnigapi hena
hetanhan waniyetu nomnom unpi kta; tuka tohan makoce
piya yuxpaxpapi kinhan hehan ake iyurpa wancake piya
wicakarnigapi kta ece.

Oehde 25. Woope kaga omniciye Oksape qa Wicakarnigapi
kin hena Minnesota Makoce kin en wakarnir
unpi ece kin hetanhanpi kta, qa wicakarnigapi xni itokam
wi ake nonpa qa sanpa Minnesota en unpi kta, qa
makoce yuxpaxpapi etanhan wicakarnigapi kte cin wi
xakpe en ounyanpi kta.

Oehde 26. Makoce kin detanhan Isantanka Omniciye
tanka yuhapi, Wicarinca eciyapi, tona en opapi kte cin
Woope kaga omniciye napin yuwitaya wicakarnigapi kta,
tohan qa token woope on wakiconzapi kin he eciyatanhan.

Oehde 27. Woope kagapi kin otoiyohi taku wanjidan
en kapi kta, qa he woope caje kin en tanin kta.

Oehde 28. Woope kaga omniciye kin Kicipaganpi
Wowapi kin kagapi kta okihi kte xni.

Oehde 29. Woope kaga omniciye kin napin tona en
opapi qa tona taokiye wicayapi kin owasin hinarin taku
econpi xni itokam, Isantanka Woope Itancan yuhapi, qa
Minnesota Makoce en Woope Itancan kin, napin wicakeyahan
opapi qa eciyatanhan oranyanpi kta e Taku
Wakan cajeyan eyapi qa en caje oiçiwapi kta.

Oehde 30. Woope kaga omniciye kin tuwe karnigapi
eca taninyan cajeyatapi qa hecen karnigapi ece e kta;
qa token cajeyan karnigapi kin owasin anpetu wowapi
kin en kagapi kta.

Oehde 31. Woope kaga omniciye kin oeconna econpi
kin tohinni yuowotanpidan kte xni, qa econpidan wowapi
kin tohinni wiyopeyepica kagapi kte xni.


# IZAPTAN.

# WAECONWICAKIYAPI KIN.

Oehde 1. Waeconwicakiyapi kin Wicaxtayatapi Itancan,
qa Wicaxtayatapi Iyokihe qa Wowapi kaga Itancan,
qa Wayawa, qa Mazaska opiye Awanyake, qa Woope
iyukcan Itancan, hena eepi kta, qa hena owasin oyate
kin wicakarnigapi kta.

Oehde 2. Tona nakaha cajewicayatapi kin wicakarnigapi
kinhan wicakarnigapi wowapi owasin Wowapi
kaga Itancan eciyapi kin he kahipi kta; unkan hena iye
Woope kaga omniciye wanji Wicakarnigapi eciyapi
Itaucan yuhapi kin he iyokosan kai kta; unkan iye
wowapi kin hena owasin yurdoke ça Woope kaga omniciye
napin okonwanjidan witaya unpi kin en taninyan
yawa qa toketu hecinhan oyake kta, omniciye kin ihduxtanpi
iyohakam yamni can ihunnipi xni.

Oehde 3. Wicaxtayatapi Itancan qa Wicaxtayatapi
Iyokihe kin hena waniyetu nonpa hehanyan en unpi kta,
qa wiciyohakam en unpi kte cin wicakarnigapi xni qa
wicayuxtanpi xni hehanyan kta. Hena wanna waniyetu
wikcemna nonpa sanpa zaptanptan kte ça wicakarnigapi
xni itokam Minnesota Makoce kin waniyetu wanjidan
qa sanpa en tipi kta. Qa napin Isantanka wicaxta wicayawapi
kin hecapi kta.

Oehde 4, Woope kaga omniciye kin tohan mniciyapi
eca Wicaxtayatapi Itancan token iyecetu dake cinhan,
makoce qa oyate kin on taku owicakiyaka ece e kta.
Akicita unpi kin he iye Itancan kte ça okicize yuke
cinhan tona cin kinhan ope wicaxi kta, qa nakun on
woope kin yuecetu ece e kta. Waeconwicakiyapi iye
ihukuya unpi kin hena otoiyohi iye oranpi kin ekta
wowapi on wiwicawange kta okihi; qa oyate kin en
tuwe xicaya oranye cin on yacopi kin he yuxke kta
okihi; tuka Woope kaga omniciye kin tuwe yacopi kinhan
Wicaxtayatapi Itancan yujuju kta okihi kte xni.
Woope kaga omniciye Oksape eciyapi kin he kici Minnesota
en Wowapi Awanyake wan kaga okihi kta, qa
nakun Wowapi yutanwicakiyapi qa tona woope on kar
xipi kin hena owasin; nakun Makoce kin en makoce
wowapi qa taku hececa Yutanwicakiyapi kin heca wicakage
kta Woope kaga omniciye kin woope tona kagapi
kin hena Wicaxtayatapi Itancan yujuju okihi kta, Woope
Itancan kin de en oyakapi iyececa. Qa tohan taku
tokeca kta seca hecinhan Woope kaga omniciye napin
wicakico okihi kta. Woope kin iyurpa awicakehan
yuecetu wicakiye kta; qa Wowapi kaga Itancan, Mazaska
opiye Awanyake, Wayawa, Woope iyukcan Itancan, qa
nakun tona woope on wicakagapi kin hena wanji en un
xni kinhan tuwe tokeca en ehnake kta okihi, qa tohanyan
oyate kin toktokeca wicakarnigapi xni hehanyan hena en
unpi kta.

Oehde 5. Wowapi kaga Itancan, Mazaska opiye
Awanyake, qa Woope iyukcan Itancan kin hena waniyetu
nomnom en unpi kta. Qa Wayawa eciyapi kin he
waniyetu yamni en un kta; qa owasin tona wiciyohakam
en unpi kta wicakarnigapi xni qa wicayuxtanpi xni
hehanyan en unpi kta. Woope Itancan kin de on
Wicaxtayatapi Itancan tokaheya kagapi kin he waniyetu
eca mazaska kektopawinge nonpa sanpa opawinge zaptan
yuhe kta. Wowapi kaga Itancan kin he waniyetu eca
kektopawinge sanpa opawinge zaptan icu kta. Wayawa
eciyapi, Mazaska opiye Awanyake, qa Woope iyukcan
Itancan, hena otoiyohi waniyetu eca kektopawingege
icupi kta. Qa tokata ekta wicaxta kin dena taku econpi
kte ça wokajuju yuhapi kte cin hena woope on
wakiconzapi kta.

Oehde 6. Wicaxtayatapi Iyokihe kin he Woope kaga
omniciye Oksape eciyapi kin he ohna Itancan yuhapi
kta; qa toketuya kaxta Wicaxtayatapi Itancan kin en un
xni kinhan he Wicaxtayatapi ee kta. Tona Oksape
omniciye en opapi token wokajuju yuhapi kin Wicaxtayatapi
Iyokihe kin he nonpa akihde yuhe kta. Qa Oksape
omniciye kin tohan enakiyapi kta hecinhan wicaxta
wanji hdarnigapi kta, qa Wicaxtayatapi Iyokihe en un
xni kinhan hee kiyapi kta.

Oehde 7. Tohan Minnesota Oyate kin Isantanka
Oyate wicayawapi kin en ope kiyapi kinhan, tona wowapi
kin de en Waeconwicakiyapi ewicakiyapi, wicakarnigapi
iyohakam, wicayuxtanpi kin hetanhan en opapi kta, qa
Witeri, anpetu wakan ihanranna tokaheya, omaka 1860,
hehanyan en unpi kta; tuka Wayawa eciyapi kin he
waniyetu 1861, hehanyan en un kta; qa nakun tona
wiciyohakam en unpi kte cin wicayuxtanpi xni hehanyan
owasin en unpi kta.

Oehde 8. Wicaxta ihdawa owasin wowapi kin de en
cajeyatapi kin hena taku econpi xni itokam, Isantanka
Woope Itancan yuhapi qa Minnesota Makoce en Woope
Itancan kin napin wicakeyahan opapi kta, qa token
econpi kta iyececa owasin tanyan econpi kta, keya Taku
Wakan cajeyatapi kta.

Oehde 9. Tohan Minnesota Oyate kin Isantanka
Oyate wicayawapi kin en ope kiyapi kin iyohakam
Woope kaga omniciye kin mniciyapi kinhan, taku tona
wowapi kin de en econpi kta keyapi kin hena hehan
wakiconzapi kta.

# IXAKPE.

## WAYACOPI KIN.

Oehde 1. Makoce kin de en Woyaco un kin, Woyaco
tanka, qa Makoce Yuxpaxpapi en Woyaco, qa Woairpeye
Woyaco, qa Wookiye Wiyukcan, qa Woyaco tona Woope
kaga omniciye kin, wicaxta yamni etanhan nomnom
cinpi kinhan, Woyaco tanka ihukuya kagapi kte cin
hena eepi kta.

Oehde 2. Woyaco tanka kin Wayaco Itancan qa
Wayaco Iyokihe nonpapi hena eepi kta; tuka Woope
kaga omniciye kin tohan iyecetu dakapi kinhan Wayaco
Iyokihe kin nakun nom wicakaga okihipi kta, wicaxta
yamni etanhan nomnom cinpi kinhan. Woyaco tanka
kin he taku tona woope on yaco xipi kin hena tokaheya
yaco kta, qa taku tona wanna yacopi tuka piya yacopi
kta cinpi kin hena en aipi kinhan, nakun owasin piya
yaco kta; tuka Woyaco kin de en wiyukcanwicakiyapi
wanice kta. Otonwe Itancan yawapi kin hen waniyetu
eca wanca qa ix nonpa wayaco unpi kta, Woope kaga
omniciye wakiconzapi kin iyececa; qa nakun Woope
kaga omniciye kin wicaxta yamni etanhan nomnom keya
wakiconzapi kinhan, Woyaco on Makoce Yuxpaxpapi kin
otoiyohi, qa wanji tukte kaxta, ohna wayaco unpi kta.
Unkan Wayaco unpi kin wicaxta wan tuwe token taku
yacopi kin oyake kta he karnigapi kta. Tona Minnesota
Makoce kin en wicaxta wicayawapi kin hena Woyaco
tanka en Wowapi kaga yanke kta e wicaxta wanji karnigapi
kta; he waniyetu yamni en un kta, qa wanji iyohakam
un kte cin karnigapi qa yuxtanpi xni hehanyan.
Qa he ihnuhanna en un xni kinhan Wayaco tankapi kin
wanji karniga okihipi kta, qa he tohanyan oyate kin
wanji karnigapi xni hehanyan en un kta.

Oehde 3. Woyaco tanka en Wayaco unpi kte cin
hena makoce owancaya wicaxta wakarnir unpi kin hena
wicakarnigapi kta; waniyetu xakowin hehanyan en unpi
kta, qa wiciyohakam unpi kte cin hena wicakarnigapi qa
wicayuxtanpi kte cin hehanyan.

Oehde 4. Woope kaga omniciye kin Minnesota Makoce
kin ocowasin etanhan Woyaco Yuxpaxpapi xakpe
kagapi kta, unkan hena Makoce Yuxpaxpapi ikceka kin
ocowasinsin etanhanhan kagapi kta, qa oyate en ounyanpi
kin hena akiyenagnakecapi kta. Unkan Woyaco
Yuxpaxpapi kin hena otoiyohi ohna, wicaxta en unpi
kin Wayaco wanjidan karnigapi kta, he woyaco econ kte
ça he nakun waniyetu xakowin en un kta. Woyaco
Yuxpaxpapi kin hena otoiyohi wicaxta ohna Wayaco
un kta karnigapi kin he makoce kin en ounye kte ça
tohanyan hecen un kin hehanyan he ohna ti kta.

Oehde 5. Woyaco Yuxpaxpapi kin en woope qa
woowotanna on woakinica owasin yacopi kta, mazaska
opawinge qa sanpa iyawapi kinhan, qa wicoran xica econpi
kin, tona on wi yamni hehanyan wicakaxkapi kta iyececa,
qa ix on mazaska opawinge sanpa kipi kta hecinhan,
he nakun yacopi kta; qa woope eciyatanhan tona piya
econpi kta keya wakiconzapi kin hena piya yacopi kta.
Woope kaga omniciye kin tohan iyecece dakapi kinhan,
Wayaco wanji Woyaco Yuxpaxpapi tokeca ekta wayaco
ye kta e woope on wakiconza okihipi kta.

Oehde 6. Woyaco Tanka qa Woyaco Yuxpaxpapi
kin en Wayaco unpi kin hena wicaxta woope onspepi
kin hecapi kta; qa Woope kaga omniciye kin token
wokajuju wicaqupi kta wakiconzapi kin he yuhapi kta,
qa tohanyan en unpi kte cin hehanyan taku wicaqupi kin
yuopten kagapi kte xni, tuka wayacopi kin on tuwe taku
wicaqu kta exta takudan icupi kte xni.

Oehde 7. Makoce kin de en Makoce Yuxpaxpapi kin
otoiyohi ohna Woairpeye Woyaco yuke kta; hena ohna
taku econpi kin owasin wowapi en kagapi kta, qa tohanhan
qa tuktekten woope eye cin hen econpi ece e kta.
Hena otoiyohi ohna Wayaco wanjidan kta, Makoce
Yuxpaxpapi iye ohna ti kin, wicaxta en ounyanpi kin
karnigapi kta, unkan waniyetu nonpa en un kta. Makoce
Yuxpaxpapi kin en ounyanpi kin etanhan wicaxta
karnigapi kta, unkan waniyetu nonpa en un kta. Makoce
Yuxpaxpapi kin en ounyanpi kin etanhan wicaxta
karnigapi kta, qa tohanyan en un kin hehanyan ohna ti
kta, qa wokajuju yuhe kte cin he woope on wakiconzapi
kta. Wowapi kaga wanji tawa kte cin kicarnigapi xni
kinhan, iye wicaxta wanji heca kage kta; tuka Woope
kaga omniciye kin tohan cinpi eca, Makoce Yuxpaxpapi
kin wanji tukte kaxta en Woairpeye on Wowapi kaga
wanji oyate kin karnigapi kta e wakiconzapi kta ece;
qa Wowapi kage cin he taku econ kte ça wokajuju taku
yuhe kte cin hena owasin woope eciyatanhan wakiconzapi
kta. Woairpeye Wayaco kin he wicaxta tapi taku yuhapi
qon hena on iyukcan kte, ça tona wamdenicapi
uncihi xni kin hena awanwicayake kta, tuka Woope
Itancan kin de en kagapi sanpa takudan econ kte xni.

Oehde 8. Makoce Yuxpaxpapi kin otoiyohi ohna
Wookiye Wiyukcan, tona en unpi kta iyececa, hena
wicakarnigapi kta e on Woope kaga omniciye kin wakiconzapi
kta; hena waniyetu nonpa unpi kta, qa taku
econpi kte ça token wokajuju yuhapi kte cin hena woope
on wakiconzapi kta. Tuka Wookiye Wiyukcan tuwedan
woakinica, mazaska opawinge akton akinicapi, qa taku
xica econpi on wicaxta wi yamni sanpa kaxkapi kta
iyececa, qa ix mazaska opawinge sanpa kipi kta iyececa,
hena qa woakinica, makoce tipi ko en un kin hena yaco
kte xni.

Oehde 9. Wicaxta Wayaco tona Woope Itancan kin
de eciyatanhan kagapi xni kin hena Woyaco Yuxpaxpapi,
qa Makoce Yuxpaxpapi, qa ix Otonwe en wayaco unpi
kte cin, wicaxta en ounyanpi kin etanhan wicakarnigapi
kta; unkan hena waniyetu xakowin sanpa unpi kte xni.

Oehde 10. Wicaxta wayaco wanji waniyetu tohanyan
en un kta e karnigapi, ihunniye xni itokam, en un xni
kinhan, Wicaxtayatapi Itancan kin wanji karnige kta;
qa he tohanyan oyate kin wanji karnigapi qa yuxtanpi
xni hehanyan en un kta. Qa en un xni iyohakam anpetu
wicakarnigapi kte cin, anpetu wikcemna yamni henakeca
kinhan, hehan oyate kin wanji karnigapi kta.

Oehde 11. Wayaco unpi kin hena Isantanka oyate
kin etanhan, qa Minnesota oyate kin etanhan wicoran
tokeca yuhapi kte xni; qa tohanyan en unpi kte cin
hehanyan oyate kin Woope Itancan kin de on taku
tokeca on wicakarnigapi exta hecetu kte xni.

Oehde 12. Woope kaga omniciye kin tohan iyecetu
dakapi kinhan, Makoce Woyaco on Yuxpaxpapi kin hena
yutokecapi kta okihipi; tuka heconpi exta on wicaxta
Wayaco kin reyata iyeyapi kte xni.

Oehde 13. Makoce Yuxpaxpapi kin en woyaco econpi
kin hena otoiyohi ohna Woyaco en Wowapi kaga wan
karnigapi kta; he taku wicaxta, qa token econ kte, ça
wokajuju taku yuhe kte cin hena woope on wakiconzapi
kta; unkan he waniyetu topa hehanyan un kta.

Oehde 14. Makoce kin de ohna woope on aiapi qa
yacopi kin hena Woope kaga omniciye kin awanyake
kta. Woyaco wowapi kagapi eca iwankam "Minnesota
Makoce," eya owapi kta; qa wiyaonpapi wowapi owasin
ehake, "Minnesota Makoce wookiye qa woyuonihan
kaxeya un kin," eya owapi ece e kta.

Oehde 15. Makoce kin de ohna Makoce Yuxpaxpapi
yuxtanpi kin otoiyohi ohna, Woope kaga omniciye kin
wicaxta wanji karnigapi kta e wakiconza okihipi kta, he
Woyaco Wiyukcan eciyapi kta, qa Wayaco Iyokihe kin
iyecen yawapi kta qa iyecen oranyan okihi kta; qa ix
Woope kaga omniciye kin Woairpeye Wayaco kin hena
hecen oranye wicakiyapi kta.

# IXAKOWIN.

# WAKARNIR UNPI KIN.

Oehde 1. Wicaxta tona waniyetu wikcemna nonpa
sam wanjidan, tona kuya obe cajeyatapi kin wanji tukte
en opapi, qa wanna Isantanka makoce kin waniyetu
wanjidan en unpi, qa wicakarnigapi kta anpetu kin
itokam wi topa makoce kin de en ounyanpi hecinhan,
hena tukten wicakarnigapi ece kin anpetu wikcemna
en unpi, oyate tona dehan qa iyohakam wicakarnigapi kte
cin owasin en opapi kta:

1st. Isantanka makoce kin en wicatonpi tona ha skapi
kin hena.

2d. Tona ha skapi makoce tokeca en icagapi, qa
Isantanka Oyate kin en woope kagapi on Isantanka wicayawapi
kte cin he eciyatanhan ihdutaninpi kinhan, hena.

3d. Tona hanke Waxicun qa hanke Ikcewicaxta,
Waxicun ihduzapi qa Waxicun wicoran opapi kin
hena.

4th. Ikcewicaxta Makoce kin de en ounyanpi, tona
*Civilization* iapi qa oihduze qa wicoran yuhapi kin hena,
Minnesota en Woyaco Yuxpaxpapi kin ohna Wayaco
yukanpi kin wanji, woope eciyatanhan token wakiconzapi
kin iyecen, wiwicawange kte, ça iye Makoce kin de
ohna wihduhapi kin yuhapi kta iyececa, keye cinhan,
hena nakun opapi kta.

Oehde 2. Tona nakaha obe cajeyatapi kin wanji
tukte en opapi xni kinhan, tona okicize kagapi qa tin
wicaktepi econpi wanna wicayacopi qa wicakiyuxkapi
xni kinhan, qa tona uncihipi xni, tona wacinhnunipi
koya, hena tuwedan Makoce kin de ohna wicaxta wicakarnigapi
kin en ope kte xni.

Oehde 3. Tuwe makoce tokeca ekta Isantanka oyate
kin taku econ kiyapi kin heon en un xni exta heon
makoce kin de en wicaxta wicakarnigapi ece kin en ope
xni kiyapi kte xni; qa tuwe makoce kin de en wakpa
ohna, qa Isantanka makoce kin en wakpa qa mde kin
ohna watopa un exta; qa tuwe waonspekiyapi kta e on
makoce tokeca ekta un exta; qa tuwe tipi ohna warpanica
wicayuhapi wan en opa exta; qa tuwe kaxka hnakapi
exta, hena on makoce kin de en wicaxta yawapi kte xni
ecinpica xni.

Oehde 4. Tuwe akicita un qa Isantanka wita wata on
zuyapi ece kin en ope cin hena makoce kin den unpi
exta heon wicaxta ounyanpi wicayawapi kte xni.

Oehde 5. Anpetu wan en wicaxta wicakarnigapi kte
cin he icunhan tuwedan oicazo akinicapi on tancan
yuzapi kte xni.

Oehde 6. Wicakarnigapi kte cin owasin wowapi
kagapi qa en ohnakapi ece e kta; tuka otonwe ohna
wicaxta wicakarnigapi kta tona togye econpi kta woope
on wakiconzapi kin henana taninyan eya cajewicayatapi
kta.

Oehde 7. Tuwe woope wowapi kin de eciyatanhan
wicakarnigapi kin en ope cin he, oun kin tona nakaha
kagapi qa tokata ekta kagapi kte cin, wanji tukte yuhe
kte cinhan, oyate kin on karnirpica kta, makoce tukten
wicakarnigapi kta anpetu wikcemna yamni en ounyan
hecinhan; tuka Woope Itancan kin de, qa Isantanka
woope ikceka qa Woope Itancan yuhapi kin en taku
togye kagapi kin he de yutokeca kte xni.

# IXAHDOGAN.

# WAONSPEKIYAPI, WOYAWA ON EHNAKAPI QA WOONSPE.

Oehde I. Oyate kin Wokiconze kagapi kinhan, wicaxta
owasin woonspe yuhapi kin on suta kta, heon etanhan
Woope kaga omniciye kin makoce owancaya xiceca
iyurpa waonspewicakiyapi kta e wakiconzapi kta.

Oehde 2. Makoce kin de ohna Makoce Tonwanyanpi
kin otoiyohi en makoce on waonspekiyapi kta Isantanka
Oyate wanna unqupi qa nakun tokata ekta unqupi kte
cin, hena wiyopeyapi kinhan, mazaska on icupi kin he
on waonspewicakiyapi kta ohinniyan yanke kta; unkan
waniyetu nonpa kinhan, makoce kin hena yamni kiyuxpapi
kta, qa etanhan wanjidan wiyopeya okihipi kta,
qa waniyetu zaptan kinhan ake kiyuxpapi wanjidan
wiyopeya okihipi kta, qa tona okaptapi kin hena waniyetu
wikcemna hehan wiyopeya okihipi kta; tuka makoce
kin tukte e ota iyawapi kin hena tokaheya wiyopeyapi
kta, qa panyehan eceedan wiyopeyapi kta. Unkan mazaska
tona, makoce qa taku tokeca waonspewicakiyapi
kta on unqupi kin hena wiyopeyapi etanhan kamnapi kin
he ohinniyan yutanpica kte xni qa yutokecapica kte xni;
tuka hetanhan oicage kin he Makoce kin en Makoce
Tonwanyanpi kin ohna, xiceca waniyetu zaptan qa wikcemna
nonpa sanpa wanjidan, tona iyotahedan unpi kin
hena iyacinyan kicipamnipi kta, qa he waonspewicakiyapi
kta ecedan on yusotapi kta.

Oehde 3. Woope kaga omniciye kin oyate kin wamnaye
wicakiyapi kte, ça ix togye econpi on mazaska
mnayanpi kta, on mazaska waonspewicakiyapi kta ehnakapi
etanhan oicage kin he koya Minnesota en Makoce
Tonwanyanpi kin otoiyohi ohna xiceca owasin tanyan
waonspewicakiyapi kta e woope kagapi kta.

Oehde 4. Minnesota Woyawa Tipi Tanka wanna
woope on kagapi kin he oyanke kin wanna detanhan
yusutapi, qa Minnesota Makoce kin en Woyawa Tipi
Tanka kin hee kta detanhan yuotaninpi. Taku tona
wanna qupi qa yuhe kiyapi kin hena owasin Woyawa
Tipi kin en detanhan yusutapi, qa Isantanka Omniciye
Tanka makoce onxpaxpa qupi, qa tona tokata ekta qupi
kta nacece cin hena owasin tawa kta.

# INAPCINWANKA.

## MAKOCE KIN EN MAZASKA MNAYANPI QA MAZASKA OPIYE.

Oehde 1. Makoce kin de ohna mazaska mnayanpi
kin tohanyan econpi eca akidecen kajujuwicakiyapi kta,
qa Makoce kin owancaya woyuha ocaje owasin mazaska
iyawapi kta, qa owotanna econpi kta.

Oehde 2. Waniyetu otoiyohi Woope kaga omniciye
kin mazaska mnayanpi, tona Makoce en yusotapi kta
iyececa he wakiconzapi kta ; qa tohinni waniyetu wanjidan
en wamnayanpi kin isanpa yusotapi hecinhan, hehan
iwaniyetu kinhan Woope kaga omniciye kin mazaska
mnayanpi kin tona waniyetu wanjidan yusotapi kta
iyececa, qa icazopi hdajujupi kta he koya, iyawapi qa
wakiconzapi kta.

Oehde 3. Woope on wakiconzapi kta, on, mazaska
owasin, oicazo, oicazo wowapi, mazaska hnakapi, wicaxta
witaya taku opetonpi, qa wicaxta tan atayedan
woyuha, tipi, makoce ko, yuhapi kin hena mazaska
iyawa eciyatanhan mnayanpi kta; tuka wicarapi kta
makoce, tipi ohna waonspekiyapi, tipi ohna wayazankapi
wicayuhapi, qa woyawa tipi ocaje owasin, tipi wakan
owasin, qa makoce Taku Wakan oie oyakapi kta on
yuhapi, ocowasin wowaonxida tipi, qa taku oyate kin
witaya yuhapi qa unpi, qa wicaxta tan atayedan tonpi
mazaska opawinge nonpa sanpa iyawapi xni kin hena
owasin woope kagapi on mazaska etanhan omaka eca
mnayanpi kte xni.

Oehde 4. Mazaska omnayan tipi qa mazaska omnayan
yuhapi kin owasin, mazaska wowapi kagapi, qa mazaska
icazo wicakiyapi, qa taku woyuha, tipi, makoce, qa taku
kaxta yuhapi kin, hena owasin etanhan, omaka eca,
mazaska mnayanpi kta e on woope kagapi kta, hecen
mazaska omnayan yuhapi kin taku tona unpi kin owasin,
wicaxta tan atayedan taku tonpi kin iyecen on mazaska,
omaka eca, mnayanpi ece e kta.

Oehde 5. Taku itokam sdonyapi xni kin on mazaska
yusotapi kta iyececa hecinhan, Oyate kin mazaska odote
kta; tuka tona hecen odotapi yuptahnakapi kinhan,
tohinni kektopawinge ecer opawinge nonpa sanpa kektopawinge
wikcemna zaptan isanpa kte xni; wodotapi kin
hena otoiyohi woope on wakiconzapi kta, qa he taku
wanjidan on econpi kta; qa woope heca kagapi eca,
Woope kaga omniciye kin napin wicaxta yamni etanhan
nomnom cinpi, qa Han qa Hiya eyapi kin, wowapi en
kagapi kta, qa hecen yucopi kinhan hehan hecetu kta.
Unkan woope hececa otoiyohi, omaka eca oicage kajujupi
kta e on womnayan en un kta, qa nakun waniyetu
wikcemna hehan ocowasin kajujupi kta token econpi kta
he nakun en un kta, qa womnaye kin he taku tokeca en
ayepica kte xni, qa yujujupica kte xni, tohanyan oicazo
ocowasin kajujupi xni hehanyan, Makoce kin de ohna
taku kagapi kte cin on tohinni Oyate kin oicazo kagapi
kte xni; qa tohinni taku econpi kin en Oyate ocowasin
on owicakiyapi kte xni; tuka taku econpi kta on makoce
qa taku yuhapi tokeca iyog ehnakapi kinhan, hena en
ayapi kta, qa etanhan oicage kin taku kagapi yuxtanpi
kta on iyog ehnakapi ece e kta.

Oehde 6. Oicazo tona nakaha cajeyatapi qa woope
on wakiconzapi kin hena owasin on wowapi, mazaska
opawinge zaptanptan iyotpani xni iyawapi kin heca
kagapi kta, oicage yuke kte ça waniyetu wikcemna
hehan kajujupi kta, hena on mazaska icazopi kta; qa
wowapi kin hena tohinni iyotpani on wiyopeyapi kte xni.
Mazaska Opiye Awanyake eciyapi kin he wowapi kin
hena owasin iyawa wowapi wan en kage kta, tuwewe
wicaqupi, qa tona kajujupi xni, heciyatanhan owasin
tanin kta.

Oehde 7. Oehde izaptan qa ixakpe ohna, tohan Oyate
kin de icazo econ kta hecinhan, heceedan ohna econpica
kta; tuka okicize yuke cinhan hehan togye econpica
kta.

Oehde 8. Oicazo econpi kin etanhan mazaska icage
cinhan, taku on icazopi kta wakiconzapi qon he en
ayapi kta, qa ix oicazo kajujupi kte cin he en yusotapi
kta, qa taku tokeca en ayepica kte xni.

Oehde 9. Makoce kin de en Mazaska Opiye kin
etanhan woope wakiconzapi kin heceedan eciyatanhan
mazaska icupica kta.

Oehde 10. Oyate kin de wicaxta tan atayedan qa
wicaxta witaya oranyanpi kin wanji tukte kaxta tohinni
mazaska owicaqu kte xni.

Oehde 11. Waniyetu eca, Witeri, anpetu wakan
tokaheya qa inonpa iyotahedan, Otonwe Itancan kin en,
Mazaska Opiye Awanyake eciyapi kin he, wotanin
wowapi wanji hen kagapi kin he ohna, wanihantanhan
mazaska tona Opiye kin etanhan icupi kin oyaka ece e
kta; qa taku on icupi, tuwewe wicaqu, qa woope tukte e
eciyatanhan econpi, hena oyake kta; qa mazaska tona
en ohnakapi qa tokiyatanhan icupi he nakun owotanna
hdaheya oyake kta.

Oehde 12. Woope kaga omniciye kin woope kagapi
kta, on mazaska Oyate tawapi qa mazaska on waonspekiyapi
kte cin tanyan yuhapi kta; qa wicaxta tona yuhapi
qa awanyakapi kin hena owotanna oranyanpi kta e on
mazaska ehnakapi kta; qa mazaska qa mazaska iyawa
tona icupi qa wicaqupi kin owasin wowapi en owotanna
kagapi kta. Unkan wicaxta kin hena wanji mazaska
kin dena etanhan onge yusote cinhan, qa econ xipi xni
kinhan tokan owicaqu hecinhan, qa ix Mazaska omnayan
toktokeca ekta ehnake, ça ix tokiyopeye cinhan, hecen
econpi kin he Oyate mazaska tawa manonpi kin hee, qa
heon teriya kuwapi kta iyececa. Unkan tuwe mazaska
Oyate tawa qa waonspekiyapi on ehnakapi kin awanyake
cin, tohan kicapi, wancahna hiyuye xni kinhan, hetanhan
wanna manon sdonyapi kta.

Oehde 13. Woope kaga omniciye kin wicaxta yamni
etanhan nomnom cinpi kinhan, Mazaska Omnaye Woope
wanji kagapi kta, qa he kaketu kta:

1st. Wicaxta tona mazaska wowapi kagapi kin hena
wicakicupi eca mazaska rinca on hiyuyapi kte xni e,
Woope kaga omniciye kin tohinni woope kagapi on, he
yuecetu kta okihipi kte xni.

2d. Woope kaga omniciye kin woope wanji kagapi kta,
on mazaska wowapi kagapi qa wiyopeyapi kin owasin
wowapi en iyawa kagapi kta; qa Isantanka mazaska
iyawa yuhapi qa Minnesota Oyate mazaska iyawa yuhapi
kin etanhan, wicaxta tona mazaska wowapi kagapi hena
e opetonpi kta, qa heon etanhan, tohan mazaska wowapi
wicakicupi eca, mazaska rinca on opekitonpi ece e kta;
qa mazaska iyawa kin hena kun aye cinhan, mazaska
Omnaye kin tona mazaska iyawa yuhapi kinhan sanpa
opetonpi ece e kta.

3d. Mazaska Omnaye mazaska wowapi kagapi kta on
kagapi kin tona icazopi kinhan wicaxta en opapi kin tan
atayedan kajujupi kta, qa heconpi kta on wicaxta otoiyohi
taku ton kin etanhan tona Omnaye en erpeye cin nonpa
akihde on icupi kta; qa he wiyopeya exta waniyetu
wanjidan iyohakam oicazo kin hena hehanyan kajuju
kiyapi kta okihipi.

4th. Mazaska Omnaye kagapi kin icazopi hdajujupi
kta ocowasin okihipi xni kinhan, mazaska wowapi kagapi
qon tona wicaxta tokeca yuhapi kin hena e tokaheya
hdajujupi kta, hehan icazopi tokeca hdajujupi kta.

5th. Mazaska Omnaye kagapi kta e on woope kagapi
kinhan, nakun wicaxta tona hena kagapi cajepi kin
owasin wowapi wan en kagapi kta, qa tona tawapi kin
atayedan en kagapi kta, qa tohan wiyopeyapi qa tuwewe
econpi kin owasin wowapi en kagapi kta, keya wakiconzapi
kta.

# IWIKCEMNA.

# WICAXTA WITAYA ORANYANPI KIN.

Oehde 1. Wicaxta witaya oranyanpi kin hena wicaxta
tan atayedan unpi kin iyecen yawapi kta, icazopi kin
kajuju wicakiyapi kta, qa iye nakun hdajuju kiyapi kta,
woope eciyatanhan.

Oehde 2. Wicaxta witaya oranyanpi kta takudan
atayedan wakiconzapi kte xni, tuka tonwanyanpi kte cin
heceedan atayedan woope on wakiconzapi kta.

Oehde 3. Wicaxta tona wicaxta witaya oranyanpi kin
en opapi kin otoiyohi, tona atayedan tawapi hehanyan,
icazopi yuke cinhan, hdajujupi kta.

Oehde 4. Wicaxta witaya oranyanpi kta kagapi kin
canku kagapi kta hecinhan, on makoce canku ohna
wanke kte cin he icupi kta; tuka makoce icupi kin on
owotanna kajujupi kta; qa wicaxta witaya oranyanpi kin
watokxupi kta hecinhan, hena taku owasin oyate yuhapi
kin towicakicikxupi kta, qa wokajuju owotanna ecedan
icupi kta.

# IAKEWANJIDAN.

## MAKOCE YUXPAXPAPI QA TONWANYANPI KIN.

Oehde 1. Woope kaga omniciye kin tohan iyecetu
dakapi kinhan, Makoce Yuxpaxpapi tokeca kaga okihipi
kta; tuka Makoce Yuxpaxpapi kagapi kte cin otoiyohi
wiyutapi hanska omdoton opawinge topa iyotpani en un
kte xni; qa tona wanna kagapi kin hena sanpa tanka
karpica kta, tuka wiyutapi hanska opawinge topa ihukuya
kagapi kte xni. Qa Makoce Yuxpaxpapi wanna kagapi
kin hena yutokecapi kta ehantanhan woope on heconpi
kte cin he oyate en ounyanpi karnigapi kinhan hecetu
kta, tuka cinpi xni kinhan woope kin he atakuni kte
xni.

Oehde 2. Tohan Otonwe wanji ohna wanna wicaxta
kektopawinge wikcemna nonpa en ounyanpi kinhan, qa
Makoce Yuxpaxpapi Otonwe en un kin, wicaxta tona en
ounyanpi kin hena cinpi kinhan, hehan Woope kaga
omniciye kin Otonwe kin he eceedan Makoce Yuxpaxpapi
wan kaga okihipi kta.

Oehde 3. Makoce Yuxpaxpapi ohna makoce en
tonwanyanpi ece ocowasinsin xni exta, otonwe wicoran
opapi kta e heon Woope kaga omniciye kin woope
kagapi kta, qa on hena yusutapi kta; tuka makoce ohna
tonwanyanpi kin naharin wicaxta opawinge en ounyanpi
xni kinhan, he tonwanyanpi tokeca wan kici yawapi kta.

Oehde 4. Makoce Yuxpaxpapi ohna, qa makoce
tonwanyanpi ohna, wicaxta ihdawa tona en unpi kta hena
wicakarnigapi kta e woope on wakiconzapi kta.

Oehde 5. Makoce Yuxpaxpapi qa tonwanyanpi kin
tona yuxtanpi kin hena woope eciyatanhan, omaka eca,
mazaska mnayanpi kta okihipi.

Oehde 6. Makoce Yuxpaxpapi qa tonwanyanpi mazaska
opiye yuhapi kin etanhan woope eciyatanhan
eceedan mazaska icupica kta.

# IAKENONPA.

## AKICITAPI KIN.

Oehde. Makoce kin de ohna wicaxta akicita wicakagapi
qa onspewicakiyapi kta e on, tohan iyecece cinhan,
Woope kaga omniciye kin woope kagapi kta.

# IAKEYAMNI.

# WIYAONPAPI QA REYATA IYEYAPI KIN.

Oehde 1. Wicaxtayatapi Itancan, Wowapi kaga Itancan,
Mazaska Opiye Awanyake, Wayawa, Woope iyukcan
Itancan, qa Woyaco Tanka Woyaco Yuxpaxpapi ko en
Wayacopi kin, hena xicaya econpi hecinhan wiyawicaonpapi
kta; tuka hecen wicayacopi exta, reyata iyewicayapi
qa icimana en unpi kta okihipi kte xni wicakage kta
heceedan. Tuka iyohakam woope on wiyawicaonpapi,
wohdag wicakiyapi, wicayacopi qa kakixwicayapi kta
okihipica kta.

Oehde 2. Wicaxta ihdawa unpi kin toktokeca, tona
taku econpi kta tuka econpi xni, qa xicaya econpi hecinhan,
token reyata iyewicayapi kta e Woope kaga omniciye
kin wakiconzapi kta.

Oehde 3. Wicaxta ihdawa wanji wiyaonpapi qa hinarin
yuowotanpi xni kinhan, icunhan taku econ ece kin hena
econ kte xni.

Oehde 4. Wicaxtayatapi Itancan kin wiyaonpapi qa
wayacopi kta hecinhan, Wicaxtayatapi Iyokihe kin he en
ope kte xni.

Oehde 5. Tuwe wiyaonpapi kin yacopi kta hecinhan,
itokam anpetu wikcemna nonpa ecetu, taku iyaonpapi
kin wowapi en kagapi qa qupi kta, qa heconpi xni kinhan
anpetu kin he en yacopi kte xni.

# IAKETOPA.

## WOOPE ITANCAN KIN TOKEN PIYA KAGAPI KTA.

Oehde 1. Tohan Woope kaga omniciye napin Woope
Itancan <!-- unclear > -->kin<!-- < unclear --> de piya kagapi qa togye kagapi kta iyecece
dakapi kinhan, token piya kagapi kte ça yutokecapi kta
cinpi kin hena atayedan cajeyatapi kta, qa hena woope,
omaka kin he en kagapi kin, aopeya oyakapi kta; qa
piya kagapi kte cin hena oyate kin iyukcanpi qa karnigapi
kta. Unkan wakarnir ipi kin wicota iyecetu dakapi
keyapi hecinhan, piya kagapi kte cin hena, qa tona
yutokecapi kte cin hena hecetu kta, qa hetanhan hena e
Woope Itancan kin de en ope kta. Anpetu wanjidan en
piya kagapi kta nonpa, qa yutokecapi kta nonpa karnigapi
kta hecinhan, kinnukankiya karnigapi kta.

Oehde 2. Tohan Woope kaga omniciye napin wicaxta yamni
etanhan nomnom Woope Itancan kin de ocowasin
piya kagapi kta e on wicaxta wicakarnigapi kta iyecetu
dakapi kinhan, hehan tohan oyate kin Woope kaga
omniciye en opapi kte cin ake wicakarnigapi kinhan,
Omniciye hececa cinpi hecinhan heyapi kta e wakiconzapi
kta. Unkan wicaxta tona wakarnir ipi kin
etanhan wicota Omniciye kin he karnigapi kinhan, hehan
Woope kaga omniciye kin tohan ake mniciyapi kinhan,
woope on Omniciye kin he kagapi kta e wakiconzapi
kta. Unkan Omniciye kin he wicaxta tona Woope kaga
omniciye kin, unma Wicakarnigapi eciyapi kin, he ix
iyenakeca kta, qa iyecen wicakarnigapi kta; unkan
wicakarnigapi iyohakam, wi yamni iyotpani, taku econwicaxipi
kin econpi kta e mniciyapi kta.

# IAKEZAPTAN.

Oehde 1. Makoce kin de ohna Wokiconze Oyanke
kin Imnijaskadan Otonwe kin hee kta; tuka Woope
kaga omniciye kin tokaheya mniciyapi kinhan, qa ix
tokata ekta, token oyate kin Wokiconze oyanke kin
tokan ayapi kta karnigapi kta he wakiconzapi kta; qa
ix, iye atayedan, makoce Isantanka Oyate kin Wokiconze
oyanke kta e on unqupi kin, he ekta ayapi kta. Unkan
Wokiconze oyanke kin Imnijaskadan etanhan tokan
ayapi kinhan, Omniciye Tipi qa makoce ihdukxan kin
hena Wookarnige, Woonspe qa Woecon on yuhapi kta,
qa hecen Woope kaga omniciye kin wakiconzapi kta;
unkan Minnesota Woyaka Omniciye kin ohinniyan he en
ope kta.

Oehde 2. Waxicun tona Ikcewicaxta Makoce kin de
ohna makoce yuhapi kin akan unpi kin hena wicaxta
wicayawapi iyecen unpi kta, tokan unpi kin iyececa,
qa ix eya omaka eca mazaska mnayanpi kin en opapi
kta.

Oehde 3. Woope kaga omniciye kin wicaxta wakarnir
unpi kin token Taku Wakan cajeyan econpi kta he
wakiconzapi kta, qa tuwe wakarnir econpi kin en ope
kte cinhan togye eye kiyapi kte xni.

Oehde 4. Minnesota Makoce kin en Wipuspe wanji
yuke kta, qa ohna Wowapi kaga Itancan yuhapi kin he
he yuha un, qa tohan taku kaga eca he un ece e kta;
unkan Wipuspe kin he Minnesota Makoce Ipuspe Tanka
kin eciyapi kta, qa Wicaxtayatapi Itancan kin wowapi
kaga eca he en un ece e kta, tuka Woope kaga omniciye
kin woope kagapi yutan kte cin hena en ope kte xni.
Wipuspe kin he token kagapi kta Woope kaga omniciye
kin wakiconzapi kta.

Oehde 5. Wicokaxke tipi wanna woope on yuxtanpi
kin he, Woope Itancan kin de yusutapi kinhan, Minnesota
Makoce kin ohna Wicokaxke tipi yuhapi kta wanji ee
kta.

## H. H. SIBLEY,

## A. D. BALCOMBE,

*Woope Itancan kagapi Omniciye kin ohna Itancanpi kin.*

# WOAHOPE WIKCEMNA KIN.

1. Mitokam taku wakan tokeca duhe kte xni.

2. Wakagapi kin takudan niçicage kte xni; wankan
marpiya kin ekta, qa kuya maka kin den, qa maka kin
ihukuya mini mahen, taku iyeye cin owasin hena okagapi
kin itokam canpexka makehde inayajin kte xni qa wakan
yada kte xni.

Jehowa nita Wakantanka he miye, Taku Wakan
winawizi waun, tona xice madapike cin hena ate yapi
wartanipi kin he cincapi kin iciyamni qa icitopa hehanyan
en awicawai ece, qa tona waxte madapike cin qa
mitawoahope opapi kin hena kektopawinge hehanyan
onxiwicawakida ece.

3. Jehowa Wakantanka nitawa kin ikcekceya caje
date kte xni. Tuwe ikcekceya caje yate cinhan he
xicaya econ Jehowa ecankin kta.

4. Anpetu okirpapi kin he kiksuye ça wakan da wo.
Anpetu xakpe rtayani qa nitortani kin owasin ecanon
kta. Tuka anpetu ixakowin kin he anpetu okirpapi,
Jehowa taku wakan yada kin he tawa, he en wicortani
takudan ecanon kte xni, niye qa nicinkxi, nicunkxi,
wicaxta nitaokiye, winyan nitaokiye, nitawoteca, qa tuwe
tokeca nitatiyopa kin en un kin henakiya.

Anpetu xakpe en Jehowa marpiya, maka, miniwanca,
qa taku ohnaka ko owasin kaga; unkan anpetu ixakowin
kin he en okirpa, hecen Jehowa anpetu okirpapi kin
hdawaxte qa hduwakan.

5. Niyate nihun kici wicahduonihan wo; hecetu kinhan
makoce Jehowa nita Wakantanka niçu kin he en tehan
yani kta.

6. Tin wicayakte kte xni.

7. Wawicirarapi ecanon kte xni.

8. Wamayanon kte xni.

9. Nitakoda on takudan oitonxni odake kte xni.

10. Nitakoda ti kin cantiheyaye kte xni, qa tawicu, qa
wicaxta taokiye, winyan taokiye, tatanka tawa, taxunke,
qa taku tokeca tawa kin owasin takudan cantiheyaye kte
xni.

# ITANCAN TAWOCEKIYE KIN.

Ate unyanpi marpiya ekta nanke cin; Nicaje kin
wakan dapi kte; Nitokiconze kin u kte. Marpiya ekta
token nitawacin econpi kin, maka akan hecen econpi
nunwe. Anpetu kin de taku yutapi unqu po; qa
waunrtanipi kin unkicicajuju po, unkix iyecen tona
ecinxniyan unkokiciranyanpi hena iyecen wicunkicicajujupi
kin. Taku wawiyutanyan un kin he en iyaye
unyanpi xni po, qa taku xica etanhan eunhdaku po.
Wokiconze kin, wowaxake kin, wowitan kin, henakiya
owihanke wanin nitawa nunwe.
"""

