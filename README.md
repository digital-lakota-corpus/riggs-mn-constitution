# Riggs MN Constitution

## License

Copyright 2022 Digital Lakota Corpus

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Description and Holding Information

1857, Constitution (1858) (Dakota)
The 1857 Constitution of Minnesota in the Dakota Language, printed in Boston
in 1858. (Digitized from a microfilm copy of title originally held by the
Library of Congress).

## MARC 21 Catalog Information

```
001 - on1164567398
003 - OCoLC
005 - 20200717185444.9
006 - m o d
007 - cr |||||||||||
008 - 200712s1858 mau ol 000 0 eng d
035 - (OCoLC)1164567398
037 - 42-008 LLMC
040 - LLMC3 eng rda pn LLMC3
043 - n-us-mn
049 - MAIN
110 - Minnesota.
240 - Constitution (1857). Dakota
245 - The constitution of Minnesota, in the Dakota language / translated by Stephen R. Riggs A.M., by the order of the Hazelwood Republic.
264 - Boston : Press of T.R. Marvin & Son, 1858.
300 - 1 online resource (36 pages)
336 - text txt rdacontent
337 - computer c rdamedia
338 - online resource cr rdacarrier
588 - Online resource; title from PDF title page (LLMC Digital, viewed July 12, 2020).
590 - LLMC3 Added to collection customer.75147.1
650 - Constitutions Minnesota.
650 - Dakota language Texts.
650 - Dakota language. fast (OCoLC)fst00887155
655 - Constitutions. fast (OCoLC)fst01774254
655 - Constitutions. lcgft
700 - Riggs, Stephen Return, 1812-1883.
710 - Hazelwood Republic.
776 - Print version: Minnesota. Constitution of Minnesota, in the Dakota language. Boston, Press of T. R. Marvin & son. 1858 (DLC) 10013825 (OCoLC)19320073
856 - LLMC Digital http://llmc.com.ezp3.lib.umn.edu/searchResultVolumes2.aspx?ext=true&catalogSet=42008
994 - 92 LLMC3
```

